<?php include("header.php"); ?>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<?php include("php/SessionUnset.php"); ?>
<?php include("banner_inner.php"); ?>
<div class="container contact_container">
  <div class="row">
    <h2>Contact</h2>
    <div class="col-md-6">
      <form action="" method="post">
        <div style="margin-top: 25px" class="form-group">
          <label>Full Name :</label>
          <input type="text" name="fullname" value="" class="form-control" placeholder="Full Name" required />
        </div>
        <div style="margin-top: 25px" class="form-group">
          <label>Email :</label>
          <input type="email" name="email" value="" class="form-control" placeholder="Email" required />
        </div>
        <div style="margin-top: 25px" class="form-group">
          <label>Subject :</label>
          <textarea cols="5" class="form-control" name="subject" placeholder="Subject" required></textarea>
        </div>
        <div style="margin-top: 25px" class="form-group">
          <input type="submit" name="submit" value="Submit" class="btn btn-success controls">
        </div>
      </form>
    </div>
    <div class="col-md-6">
      <center>
        <h2>Address</h2>
      </center>
      <center>
        <h2>1234567890</h2>
      </center>
      <center>
        <h2>taxi@yoursite.com</h2>
      </center>
    </div>
  </div>
</div>
<?php include("footer.php"); ?>
