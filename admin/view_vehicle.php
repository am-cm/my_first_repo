<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<?php include('header.php'); ?>
                    
                   
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.html">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">View</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Vehicle</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <a class="btn red btn-outline btn-primary" href="add_user.php">Create New User</a>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> Image </th>
                                                <th> Make </th>
                                                <th> Dtiver Name </th>
                                                <th> Owner Name </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $query=mysql_query("SELECT driver_vehicle_info.*, user.firstname , user.lastname FROM driver_vehicle_info JOIN user on driver_vehicle_info.user_id = user.id");
                                                while($vehicle=mysql_fetch_array($query))
                                                {
                                            ?>
                                            <tr>
                                                <td><img src="../upload/vehicle/<?php echo $vehicle['image']; ?>" width="100px"></td>
                                                <td> <?php echo $vehicle['make']; ?> </td>
                                                <td> <?php echo $vehicle['firstname'] . ' ' . $vehicle['lastname']; ?></td>
                                                <td><?php echo $vehicle['owner_name']; ?></td>
                                                <td> <a href="driver_profile.php?id=<?php echo $vehicle['user_id']; ?>" class="btn purple"> Edit
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                </div>
                <!-- END CONTENT BODY -->
</div>
    <?php include('footer.php'); ?>

 <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>