<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function()
{
    $(".LinkStatus").click(function()
    {
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        var dataString = 'lid='+ id;

        $.ajax
        ({
            type: "POST",
            url: "php/LinkStatus.php?status=" + status,
            data: dataString,
            cache: false,
            success: function(html)
            {
                window.location.assign("ViewShareLink.php");
            } 
        });
    });

    
});
</script>
<?php include('header.php'); ?>
                    
                   
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">View</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Share Link</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <a class="btn red btn-outline btn-primary" href="add_user.php">Create New User</a>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Link </th>
                                                <th> Date </th>
                                                <th> status </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $query=mysql_query("SELECT * FROM sharelink");
                                                while($link=mysql_fetch_array($query))
                                                {
                                                    $user_id=$link['user_id'];
                                                    $namequery=mysql_query("SELECT * FROM user where id='$user_id'");
                                                    $linkname=mysql_fetch_array($namequery);
                                            ?>
                                            <tr>
                                                <td><?php echo $linkname['firstname']." ".$linkname['lastname']; ?></td>
                                                <td> <?php echo $link['url']; ?> </td>
                                                <td> <?php echo $link['date']; ?></td>
                                                <td>
                                                    <?php if($link['status'] ==0){ ?>            
                                                            <i class="fa fa-check"></i>
                                                    <?php }else{ ?>
                                                            <i class="fa fa-close"></i>
                                                    <?php } ?>
                                                </td>
                                                <td> 
                                                <?php if($link['status'] ==0){ ?>
                                                        <a class="btn btn-primary LinkStatus" data-status="1" data-id="<?php echo $link['id']; ?>"  > approve
                                                            <i class="fa fa-check"></i>
                                                        </a>
                                                <?php }else{ ?>
                                                        <a class="btn btn-warning LinkStatus" data-status="0" data-id="<?php echo $link['id']; ?>"  > Reject
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                <?php } ?>
                                                    <a href="javascript:;" class="btn red">
                                                        <i class="fa fa-times"></i> Delete
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                </div>
                <!-- END CONTENT BODY -->
</div>
    <?php include('footer.php'); ?>

 <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>