<?php include("header.php"); ?>
<?php include("php/SessionUnset.php"); ?>
<?php include("banner_inner.php"); ?>

<div class="container about_container">
  <h2>About Us</h2>
  <p>An Emerging Airport Transfer Club Which Is Available 24/7 At Your Service To Fulfill Your Frequent Journey Requirement 
    At Very Competitive Rates In Very Comfortable And Friendly Environment. We Work With Prominent Taxi Firm Who Is Licensed By
    Local Authorities. We Cherry Pick Those Firms To Ensure Quality Of Service Is At Its Best. Our Goal Is To Make Your Ride 
    Pleasant Whether You Travelling Alone Or With Family. As An Airport Transfer Club, We Make Sure That Our Entire Partner’s 
    Fleet Suit Your All Needs While You Carrying Excess Luggage Or Even When You Traveling With Kids. Your Safety And Comforts
    Is Our Ultimate Goal. In Order To Do That, We Spend A Lot Time And Money To Train Our Staff And Also Examine Our Fleet Time
    To Time. Our Fleets Are Suitable For Your Entire Journey Whether Is About <b>Travelling To Airport From London Or Coming Back
    From Airport To London.</b> Our Fleet Is Fully Equipped To Make Your Journey Pleasant</p>
  <h4><b>Our View Of The Industry</b></h4>
  <p><b>Airport Transfer Club</b> As The Game Changer For The Transport Business, While All The Major Players Are Too Busy Expanding 
    And Employing More Drivers And Compromising Quality In The Process. We Not Only Aim At Getting A Person From <b>A To B</b> But We 
    Make Sure  The Time He Spends Getting From<b> A To B</b> To Match The Level Of Service Provided By <b>Us</b> So That His Whole Experience 
    Is In<b> Unforgettable</b> Section Of Customer’s <b>Memories.</b></p>
  <p>Since Cherry Pick All Our Drivers, We Assure To Match The Quality We Promise. To Maintain The Quality From Time To Time, 
    We Get In Touch With Our Customers Randomly To Get A Feedback Of Their Most Recent Journey, It Helps Us To Reward The Best 
    Fleet / Drivers & This Old School Practice Has Helped Us To Bring The Best Out Of Our Drivers And Create A Competitive Environment
    Amongst Them. We Create A Win-Win Situation Both For Customers And Our Fleet Members And <b>The Appreciation</b> We Receive For Their
    Drivers From Our Clients Is Something To Be Proud Of. They Go Over And Beyond To Put A Smile On Their Customers Face.</p>
  <p>Here At <b>ATC</b>, We Are Mainly Focussing On One Thing And That Is <b>“Customer Satisfaction”</b> And The Rest Follows.</p>
  <h3>Here is the Few Key Feature That Say ‘Why Choose To Travel With Us’.</h3>
  <ul class="about_ul">
    <li>Our All Vehicle Provided By Our Partners Are Equipped To Use <b>Advance Technology</b> To Manage Our Fleet Which Enhance
      More Accurate Response On Request.</li>
    <li>Our<b> Waiting Time Policy</b> Is Simpler Than Rest. </li>
    <li><b>Our Rates</b> Are Very Competitive In All Over London.</li>
    <li>All Our Member Drivers Wear Public Carriage Badge ID To Make Sure For <b>Your Safety</b>.</li>
    <li><b>Our Staff</b> Very Enthusiastic & Always Eager To Assistance. </li>
    <li>We Charge<b> Flat Rates</b> So No Worry In Any Kind Of Traffic Or Road Diversion Due To Accident Or Road Closure</li>
    <li>Foreign Nationals Will Have No Problem Of Finding A Currency Exchange As We Accept All <b>Major Currency</b>.</li>
    <li><b>Flights Status</b> Is Updated And Tracked Automatically Which Is Linked To The Flight Arrivals Of All Major Airports.
      Hence No Worries Of Paying Unreasonable Waiting Time. </li>
    <li><b>Helpful, Professional, Courteous And Customer Oriented</b> Drivers So The Clients Don’t Have To Worry About Pushing
      Trolleys For Loading Or Unloading Their Luggage.</li>
    <li>We Provide <b>Baby Seats</b> When Requested By The Clients To Make Their Journey Safer And Comfortable.</li>
  </ul>
</div>
<?php include("footer.php"); ?>
