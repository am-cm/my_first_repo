<?php
include('function.php');
if(isset($_POST['submit']))
{
	$title=$_POST['title'];
	$name=$_POST['name'];
	$datepicker=$_POST['datepicker'];
	$dvlano=$_POST['dvlano'];
	$pco=$_POST['pco'];
	
	$surname=$_POST['surname'];
	$ninumber=$_POST['ninumber'];
	$postcode=$_POST["postcode"];
	$dvlaexpirydate=$_POST['dvlaexpirydate'];
	$pcoexpirydate=$_POST['pcoexpirydate'];
	$address1=$_POST['address1'];
	$address2=$_POST['address2'];
	$email=$_POST['email'];
	
	$avatar=$_FILES['avatar'];
	$photo_dvla=$_FILES['photo_dvla'];
	$photo_dvla_counter=$_FILES['photo_dvla_counter'];
	$photo_pco=$_FILES['photo_pco'];
	$photo_pco_counter=$_FILES['photo_pco_counter'];


if(($avatar['type'] && $photo_dvla['type'] && $photo_dvla_counter['type'] && $photo_pco['type'] && $photo_pco_counter['type'] == 'image/gif')
|| ($avatar['type'] && $photo_dvla['type'] && $photo_dvla_counter['type'] && $photo_pco['type'] && $photo_pco_counter['type'] == 'image/jpeg')
|| ($avatar['type'] && $photo_dvla['type'] && $photo_dvla_counter['type'] && $photo_pco['type'] && $photo_pco_counter['type'] == 'image/png')
|| ($avatar['type'] && $photo_dvla['type'] && $photo_dvla_counter['type'] && $photo_pco['type'] && $photo_pco_counter['type'] == 'application/msword')
|| ($avatar['type'] && $photo_dvla['type'] && $photo_dvla_counter['type'] && $photo_pco['type'] && $photo_pco_counter['type'] == 'application/pdf')
&& ($avatar['size'] && $photo_dvla['size'] && $photo_dvla_counter['size'] && $photo_pco['size'] && $photo_pco_counter['size'] < 71680000))
{
    if($avatar['error'] || $photo_dvla['error'] || $photo_dvla_counter['error'] || $photo_pco['error'] || $photo_pco_counter['error'] > 0)
    {
        echo "retrun code:" . $avatar['error'] .$photo_dvla['error'] .$photo_dvla_counter['error'] .$photo_pco['error'] .$photo_pco_counter['error'];
    }

    else if(file_exists('upload/'.$avatar['name']) || ('upload/'.$photo_dvla['name']) || ('upload/'.$photo_dvla_counter['name']) || ('upload/'.$photo_pco['name']) || ('upload/'.$photo_pco_counter['name']))
    {

        $temp = explode(".", $avatar["name"]);
        $temp1 = explode(".", $photo_dvla["name"]);
        $temp2 = explode(".", $photo_dvla_counter["name"]);
        $temp3 = explode(".", $photo_pco["name"]);
        $temp4 = explode(".", $photo_pco_counter["name"]);
        $newfilename  = $temp[0]. '-' . time() . '.' . end($temp);
        $newfilename1 = $temp1[0]. '-' . time() . '.' . end($temp1);
        $newfilename2 = $temp2[0]. '-' . time() . '.' . end($temp2);
        $newfilename3 = $temp3[0]. '-' . time() . '.' . end($temp3);
        $newfilename4 = $temp4[0]. '-' . time() . '.' . end($temp4);
        move_uploaded_file($avatar["tmp_name"], "upload/user/" . $newfilename);
        move_uploaded_file($photo_dvla["tmp_name"], "upload/driver_document/personal/" . $newfilename1);
        move_uploaded_file($photo_dvla_counter["tmp_name"], "upload/driver_document/personal/" . $newfilename2);
        move_uploaded_file($photo_pco["tmp_name"], "upload/driver_document/personal/" . $newfilename3);
        move_uploaded_file($photo_pco_counter["tmp_name"], "upload/driver_document/personal/" . $newfilename4);

		$form_data = array(
            'firstname' => $name,
            'lastname' => $surname,
            'email' => $email,
            'address1' => $address1,
            'address2' => $address2,
            'pin' => $pco,
            'image' => $newfilename,
            'user_type' => 'driver',
            'status' => '0',
        );
         dbInsert('user', $form_data);
      
		
		$last_id=mysql_insert_id();
		
		$query=mysql_query("select * from `user` where user_type='driver' and id='$last_id'");
		$row=mysql_fetch_array($query);
		
		$last_id_fetch=$row['id'];
		
		$insert = array(
             'title' => $title,
             'dob' => $datepicker,
             'ni_no' => $ninumber,
             'dvla_no' => $dvlano,
             'pco' => $postcode,
             'user_id' => $last_id_fetch,
        );
         dbInsert('driver_personal_info', $insert);
		
		
		$insert1 = array(
             'user_id' => $last_id_fetch,
         );
         dbInsert('driver_vehicle_info', $insert1);
		
		$document_insert = array(
             'user_id' => $last_id_fetch,
             'name' => 'Photocopy of DVLA',
             'document' => $newfilename1,
             'type' => 'Personal',
             'status' => '0',
			 'expiry_date' => $dvlaexpirydate,
		);
         dbInsert('documents', $document_insert);
		 
		
		$document_insert1 = array(
             'user_id' => $last_id_fetch,
             'name' => 'DVLA Counter Part',
             'document' => $newfilename2,
             'type' => 'Personal',
             'status' => '0',
			 'expiry_date' => $dvlaexpirydate,
         );
         dbInsert('documents', $document_insert1);
		 
		 $document_insert2 = array(
             'user_id' => $last_id_fetch,
             'name' => 'PCO Badge',
             'document' => $newfilename3,
             'type' => 'Personal',
             'status' => '0',
			 'expiry_date' => $pcoexpirydate,
         );
         dbInsert('documents', $document_insert2);
		 
		 $document_insert3 = array(
             'user_id' => $last_id_fetch,
             'name' => 'PCO Counter Part',
             'document' => $newfilename4,
             'type' => 'Personal',
             'status' => '0',
			 'expiry_date' => $pcoexpirydate,
         );
         dbInsert('documents', $document_insert3);
		 
		if ($form_data and $insert and $document_insert and $document_insert1 and $document_insert2 and $document_insert3 and $insert1)
        {
            echo "successfully insert this record";
			
			?>
			<script>window.location.assign("../driver_requirement.php");</script>
			<?php
        }
		else{
			echo "not";
		}

    }

} 
}
?>