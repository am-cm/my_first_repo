<?php include('head.php'); ?>
<link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
 <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function()
{
      
    $(".state").change(function()
    {
        var id=$(this).val();
        var dataString = 'sid='+ id;
        //var=userid=document.getElementsByTagName('user_id').val();
        //var dataString = 'uid='+ userid;
    
        $.ajax
        ({
            type: "POST",
            url: "php/get_city.php",
            data: dataString,
            cache: false,
            success: function(html)
            {
                $("#city").html(html);
            } 
        });
    });
    
});
</script>

<?php include('header.php'); ?>
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Profile</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> View User Profile | Profile
                        <small>Driver account page</small>
                    </h3>
                    <?php if(isset($_SESSION['Success'])!=''){ ?>
                        <div class="alert alert-success fade in" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <strong>Success!</strong> <?php echo $_SESSION['Success']; ?>
                        </div>
                        <?php 
                        unset($_SESSION["Success"]);
                        ?>
                    <?php } ?>
                    <?php if(isset($_SESSION['error'])!=''){ ?>
                        <div class="alert alert-success fade in" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <strong>error!</strong> <?php echo $_SESSION['error']; ?>
                        </div>
                        <?php 
                        unset($_SESSION["error"]);
                        ?>
                    <?php } ?>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <input type="hidden" name="userid" value="<?php echo $id ?>" id="user_id">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar">
                                <!-- PORTLET MAIN -->
                                <div class="portlet light profile-sidebar-portlet ">
                                    <!-- SIDEBAR USERPIC -->
                                    <div class="profile-userpic">
                                    <?php if($rowtop['image']==""){ ?>
                                        <img src="..\assets\global\img\user.png" class="img-responsive" alt="">
                                    <?php }else{ ?>
                                        <img src="..\upload\user\<?php echo $rowtop['image'] ?>" class="img-responsive" alt=""> 
                                    <?php } ?>
                                        </div>
                                    <!-- END SIDEBAR USERPIC -->
                                    <!-- SIDEBAR USER TITLE -->
                                    <div class="profile-usertitle">
                                        <div class="profile-usertitle-name"> <?php echo $rowtop['firstname']; ?> </div>
                                        <div class="profile-usertitle-job"> <?php echo $rowtop['user_type']; ?> </div>
                                    </div>
                                    <!-- END SIDEBAR USER TITLE -->
                                    <!-- SIDEBAR BUTTONS -->
                                   
                                    <!-- END SIDEBAR BUTTONS -->
                                    <!-- SIDEBAR MENU -->
                                   
                                    <!-- END MENU -->
                                </div>
                                <!-- END PORTLET MAIN -->
                               
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <?php
                                $user_id=$rowtop['id'];
                                $driver=mysql_query("select * from driver_personal_info where user_id='$user_id'");
                                $d=mysql_fetch_array($driver);
                            ?>
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light ">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                </div>
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_2" data-toggle="tab">Change Image</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <form role="form" method="post" action="php/update_user_data.php" enctype="multipart/form-data">
                                                    <div class="tab-content">
                                                            <div class="tab-pane active" id="tab_1_1">
                                                                <div class="form-group">
                                                                    <label class="control-label">Title</label>
                                                                    <input class="form-control" placeholder="Mr." id="mask_date" type="text" name="title" value="<?php echo $d['title']; ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">First Name</label>
                                                                    <input type="text" name="firstname" value="<?php echo $rowtop['firstname']; ?>" placeholder="John" class="form-control" /> 
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Last Name</label>
                                                                    <input type="text" name="lastname" value="<?php echo $rowtop['lastname']; ?>" placeholder="Doe" class="form-control" /> 
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Email</label>
                                                                    <input aria-invalid="true"  value="<?php echo $rowtop['email']; ?>" aria-describedby="email-error" aria-required="true" name="email" class="form-control" type="text" placeholder="test@gmail.com">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Mobile Number</label>
                                                                    <input type="text" value="<?php echo $rowtop['mobile']; ?>" placeholder="+1 989898 DEMO (6284)" class="form-control" name="mobile" /> 
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Select Gender :</label>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline">
                                                                                <input type="radio" name="gender" id="optionsRadios25" value="male" <?php if($rowtop['gender']=="male"){ echo "checked";} ?>> Male </label>
                                                                            <label class="radio-inline">
                                                                                <input type="radio" name="gender" id="optionsRadios26" value="female" <?php if($rowtop['gender']=="female"){ echo "checked";} ?>> Female </label>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">1st line address</label>
                                                                    <input type="text" name="address1" value="<?php echo $rowtop['address1']; ?>" placeholder="Enter 1st line address" class="form-control" /> 
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">2nd line address</label>
                                                                    <input type="text" name="address2" value="<?php echo $rowtop['address2']; ?>" placeholder="Enter 2st line address" class="form-control" /> 
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">County</label>
                                                                    <input class="form-control" placeholder="Enter County..." id="mask_date" type="text" name="county" value="<?php echo $rowtop['state']; ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Town</label>
                                                                    <input class="form-control" placeholder="Enter Town..." id="mask_date" type="text" name="town" value="<?php echo $rowtop['city']; ?>">
                                                                </div>
                                                                 <div class="form-group">
                                                                    <label class="control-label">Postcode</label>
                                                                    <input type="text" value="<?php echo $rowtop['pin']; ?>" placeholder="Enter postcode" class="form-control" name="postcode" /> 
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label class="control-label">Date Of Burthday</label>
                                                                    <input class="form-control" placeholder="YYYY-MM-DD" id="mask_date" type="text" name="dob" value="<?php echo $d['dob']; ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">N I Number</label>
                                                                    <input class="form-control" id="mask_date" type="text" name="ni_no" value="<?php echo $d['ni_no']; ?>">
                                                                </div>
                                                                
                                                                <div class="form-group">
                                                                    <label class="control-label">DVLA No</label>
                                                                    <input class="form-control" id="mask_date" type="text" name="dvla_no" value="<?php echo $d['dvla_no']; ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">PCO Badge</label>
                                                                    <input class="form-control" id="mask_date" type="text" name="pco" value="<?php echo $d['pco']; ?>">
                                                                </div> 
                                                                <div class="margiv-top-10">
                                                                    <input type="hidden" name="uid" value="<?php echo $rowtop['id']; ?>">
                                                                    <button class="btn green" name="PersonalInfo" value=" Save Changes" > Save Changes </button> 
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                               
                                                            </div>
                                                            <!-- END PERSONAL INFO TAB -->
                                                            <!-- CHANGE AVATAR TAB -->
                                                            <div class="tab-pane" id="tab_1_2">
                                                                
                                                                    <div class="form-group">
                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                            <?php if($rowtop['image']!=""){ ?>
                                                                                <img src="../upload/user/<?php echo $rowtop['image']; ?>" alt="" /> </div>
                                                                            <?php }else{ ?>
                                                                                 <img src="..\assets\global\img\adduser.png" alt="" /> </div>
                                                                            <?php } ?>
                                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                            <div>
                                                                                <span class="btn default btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="image"> </span>
                                                                                <a href="javascript:;" class="btn default fileinput-exists fileinput-new" data-dismiss="fileinput"> Remove </a>
                                                                            <?php if($rowtop['image']!=""){ ?>
                                                                                <a class="btn default" href="php/delete_user_image.php?user_id=<?php echo $rowtop['id'] ?>"> Delete </a>
                                                                            <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix margin-top-10">
                                                                            <span class="label label-danger">NOTE! </span>
                                                                            <span> Maximum Uplode 2MB File (Only JPG & PNG File Supported)</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="margin-top-10">
                                                                        <button class="btn green" name="ChangeImage" value="Save Changes"> Save Changes </button>
                                                                        <a href="javascript:;" class="btn default"> Cancel </a>
                                                                    </div>
                                                                
                                                            </div>
                                                            <!-- END CHANGE AVATAR TAB -->
                                                            <!-- CHANGE PASSWORD TAB -->
                                                            <div class="tab-pane" id="tab_1_3">
                                                               
                                                                    <div class="form-group">
                                                                        <label class="control-label">Current Password</label>
                                                                        <input type="text" class="form-control" name="cpassword" value="<?php echo $rowtop['password']; ?>" /> </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">New Password</label>
                                                                        <input type="password" class="form-control" name="password" /> </div>
                                                                    <div class="margin-top-10">
                                                                        <button class="btn green" name="ChangePassword" value="Change Password"> Change Password </button>
                                                                        <a href="javascript:;" class="btn default"> Cancel </a>
                                                                    </div>
                                                              
                                                            </div>
                                                            <!-- END CHANGE PASSWORD TAB -->
                                                            <!-- PRIVACY SETTINGS TAB -->
                                                            <div class="tab-pane" id="tab_1_4">
                                                                <table class="table table-light table-hover">
                                                                    <tr>
                                                                        <td> User status... </td>
                                                                        <td>
                                                                            <!--
                                                                            <label class="uniform-inline">
                                                                                <input type="radio" name="status" value="1" <?php if($rowtop['status']==1){ echo 'checked';} ?>/> Active </label>
                                                                            <label class="uniform-inline">
                                                                                <input type="radio" name="status" value="0" <?php if($rowtop['status']==0){ echo 'checked';} ?>/> Deactive </label>
                                                                            -->
                                                                            <?php if($rowtop['status']==1){ ?>
                                                                                <h2>Active</h2>
                                                                            <?php }else{?>
                                                                                <h2>Deactive</h2>
                                                                            <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                   
                                                                </table>
                                                                <!--end profile-settings-->
                                                                <div class="margin-top-10">
                                                                    <button class="btn red" name="PrivacySettings"
                                                                    value="Save Changes"> Save Changes </button>
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </div>
                                                        <!-- END PRIVACY SETTINGS TAB -->
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
<?php include('footer.php'); ?>
<script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/select2/js/select2.full.min.js"></script>
<script src="../assets/pages/scripts/components-select2.min.js"></script>
<script src="../assets/pages/scripts/components-date-time-pickers.min.js"></script>
<script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>