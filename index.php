<?php include("header.php"); ?>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>



<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/full-slider.css" rel="stylesheet">
<link href="css/materialize2.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<link href="date_picker/css/style.css" rel="stylesheet" type="text/css"/>
<script src="date_picker/js/jquery-2.1.4.min.js"></script>
<script src="date_picker/js/bootstrap.min.js"></script>
<link href="date_picker/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="date_picker/js/moment-with-locales.js"></script>
<script src="date_picker/js/bootstrap-datetimepicker.js"></script>

<?php include("php/SessionUnset.php"); ?>
<?php include("banner.php"); ?>
<style style="text/css">
.carousel-control.left,.carousel-control.right  {background:none;width:25px;}
.carousel-control.left {left:-25px;}
.carousel-control.right {right:-25px;}

.block-text {
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 3px 0 #2c2222;
    color: #626262;
    font-size: 14px;
    margin-top: 27px;
    padding: 15px 18px;
}
.block-text a {
 color: #7d4702;
    font-size: 25px;
    font-weight: bold;
    line-height: 21px;
    text-decoration: none;
    text-shadow: 0 1px 2px rgba(0, 0, 0, 0.3);
}
.mark {
    padding: 12px 0;background:none;
}
.block-text p {
    color: #585858;
    font-family: Georgia;
    font-style: italic;
    line-height: 20px;
}

.sprite-i-triangle {
    background-position: 0 -1298px;
    height: 44px;
    width: 50px;
}
.block-text ins {
    bottom: -44px;
    left: 50%;
    margin-left: -60px;
}


.block {
    display: block;
}
.zmin {
    z-index: 1;
}
.ab {
    position: absolute;
}

.person-text {
    padding: 10px 0 0;
    text-align: center;
    z-index: 2;
}
.person-text a {
    color: #ffcc00;
    display: block;
    font-size: 14px;
    margin-top: 3px;
    text-decoration: underline;
}
.person-text i {
    color: #fff;
    font-family: Georgia;
    font-size: 13px;
}
.rel {
    position: relative;
}
.slideup_img_span img{
animation-name: slideup_img_span_img;
animation-iteration-count: 1;
animation-timing-function: ease-out;
animation-duration: 4s;
}
@keyframes slideup_img_span_img {
0% {
transform: translateY(-80px);
}
100% {
transform: translateY(0);
}
}
div.slide-up p {
animation-name: dropHeader;
animation-iteration-count: 1;
animation-timing-function: ease-out;
animation-duration: 4s;
}
@keyframes dropHeader {
0% {
transform: translateY(80px);
}
100% {
transform: translateY(0);
}
}


</style>
<section id="choose-car">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div>
        <h3 class="header header-car">CHOOSE YOUR CAR</h3>
      </div>
      <div>
        <ul class="nav nav-tabs nav-tabs-center">
          <li class="active"><a data-toggle="tab" href="#saloon">Saloon</a></li>
          <li><a data-toggle="tab" href="#estate">Estate</a></li>
          <li><a data-toggle="tab" href="#mpv">MPV</a></li>
          <li><a data-toggle="tab" href="#transporter">Transporter</a></li>
          <li><a data-toggle="tab" href="#executive">Executive</a></li>
		      
          
        </ul>
        <div class="tab-content">
          <div id="saloon" class="tab-pane fade in active"> <img alt="Saloon" src="images/saloon.png" title="Saloon">
            <div class="uk-grid uk-grid-divider uk-text-center">
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Passenger:
                  <h3 class="uk-text-white">5</h3>
                </div>
              </div>
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> luggage :
                  <h3 class="uk-text-white">UpTO 20KG(3 bags)</br>UpTO 5KG(3 bags)</h3>
                </div>
              </div>
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Class:
                  <h3 class="uk-text-white">small/ town car</h3>
                </div>
              </div>
            </div>
          </div>
          <div id="estate" class="tab-pane fade"> <img alt="Estate" src="images/estate.png" title="Estate">
            <div class="uk-grid uk-grid-divider uk-text-center">
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Passenger:
                  <h3 class="uk-text-white">UpTo 4</h3>
                </div>
              </div>
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Per Mile/ Km:
                  <h3 class="uk-text-white">UpTO 20KG(3 bags)</br>UpTO 5KG(3 bags)</h3>
                </div>
              </div>
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Class:
                  <h3 class="uk-text-white">family car</h3>
                </div>
              </div>
            </div>
          </div>
          <div id="mpv" class="tab-pane fade"> <img alt="MPV" src="images/mpv.png" title="MPV">
            <div class="uk-grid uk-grid-divider uk-text-center">
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Passenger:
                  <h3 class="uk-text-white">UpTo 6</h3>
                </div>
              </div>
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Luggage:
                  <h3 class="uk-text-white">UPTO 20KG(4 bags)</br>UPTO 5KG(4 bags)</h3>
                </div>
              </div>
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Class:
                  <h3 class="uk-text-white">suv</h3>
                </div>
              </div>
            </div>
          </div>
          <div id="transporter" class="tab-pane fade in "> <img alt="Swift" src="images/swift.png" title="Transporter">
            <div class="uk-grid uk-grid-divider uk-text-center">
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Passenger:
                  <h3 class="uk-text-white">Upto 8</h3>
                </div>
              </div>
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Luggage:
                  <h3 class="uk-text-white">UPTO 20KG(8 bags)</br>UPTO 5KG(6 bags)</h3>
                </div>
              </div>
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Class:
                  <h3 class="uk-text-white">small/ town car</h3>
                </div>
              </div>
            </div>
          </div>
          <div id="executive" class="tab-pane fade"> <img alt="Executive" src="images/executive.png" title="Executive">
            <div class="uk-grid uk-grid-divider uk-text-center">
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Passenger:
                  <h3 class="uk-text-white">UpTo 3</h3>
                </div>
              </div>
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Luggage:
                  <h3 class="uk-text-white">UPTO 20KG(2 bags)</br>UPTO 5KG(2 bags)</h3>
                </div>
              </div>
              <div class="uk-width-medium-1-3">
                <div class="uk-panel uk-panel-box uk-panel-box-primary"> Class:
                  <h3 class="uk-text-white">limousine</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<section id="lostProperty">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="counter">OUR JOURNEY</h1>
        <br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div> <img src="images/male-user.png" class="img-responsive journey_client animated fadeInDownBig" /> <span class="stat-count">528</span>
          <div class="slide-up">
            <h4 class="stat-detail animated fadeInUpBig client">Client</h4>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div> <img src="images/driver.png" class="img-responsive journey_driver animated fadeInDownBig" /> <span class="stat-count">57</span>
          <div class="slide-up">
            <h4 class="stat-detail animated fadeInUpBig driver_name">Driver</h4>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div> <img src="images/sports-car.png" class="img-responsive journey animated fadeInDownBig" /> <span class="stat-count">372</span>
          <div class="slide-up">
            <h4 class="stat-detail animated fadeInUpBig journey_name">Journey</h4>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div> <img src="images/pound-sterling.png" class="img-responsive journey_pound animated fadeInDownBig" /> <span class="stat-count transaction_amount">150</span>
          <div class="slide-up">
            <h4	class="stat-detail animated fadeInUpBig">Transaction Amount in Pound</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
</section>
<section id="downloadApp">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="header header-car">DOWNLOAD OUR APP</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div>
          <p><img class="uk-text-center" alt="download our app" src="images/app.jpg"> </p>
        </div>
      </div>
      <div class="col-md-6">
        <h4 class="text-center uk-text-black">Book your taxi in just a few second!</h4>
        <p class="text-center p-mar-5">Our app is easy to use and very user friendly. Just choose your start and end location, select the car you like and in just a few minutes we'll be at your doorstep.</p>
        <br>
        <h4 class="text-center uk-text-black p-mar-5">Taxi app key features:</h4>
        <h5 class="blocknumber blocknumber-ltr"><span class="circle">01</span> Easy taxi booking // lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h5>
        <h5 class="blocknumber blocknumber-ltr"><span class="circle">02</span> Check the rate in real time // lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h5>
        <h5 class="blocknumber blocknumber-ltr"><span class="circle">03</span> Calculate the trip mileage // lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h5>
        <br>
        <h4 class="text-center uk-text-black p-mar-5">Download Now:</h4>
        <p class="text-center p-mar-5">This app is available for: Android, iOS &amp; Windows Mobile!</p>
        <p class="text-center p-mar-15">CityCab is reserving the rights to change prices, rates, vehicles in our disgrace.</p>
        <br>
        <p class="text-center p-mar-15"> <a href="#" class="btn btn-inverse"><i class="uk-icon-apple"></i></a> <a href="#" class="btn btn-inverse"><i class="uk-icon-android"></i></a> <a href="#" class="btn btn-inverse"><i class="uk-icon-windows"></i></a> </p>
      </div>
    </div>
  </div>
</section>
<section id="verticalTestimonials" data-type="background" data-speed="5" style="background:#073A29;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="header header-testimonials">WHAT PEOPLE THINK ABOUT US</h1>
      </div>
    </div>
  </div>
<div class="carousel-reviews broun-block">
    <div class="container">
        <div class="row">
            <div id="carousel-reviews" class="carousel slide" data-ride="carousel">
            
                <div class="carousel-inner">
                    <div class="item active">
                	    <div class="col-md-6 col-sm-6">
        				  <div class="sprocket-quotes-item quotes-bottomright-arrow">
                      <p class="sprocket-quotes-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                      <div class="sprocket-quotes-info"> <img src="images/3.png" class="sprocket-quotes-image" alt="image"> <span class="sprocket-quotes-author"> Diego Furlan </span> <span class="sprocket-quotes-subtext"> Lives in: Barcelona </span> </div>
                    </div>
							
						</div>
						
            			<div class="hidden-xs">
						    
													</div>
						<div class="col-md-6 col-sm-6 hidden-sm hidden-xs">
							<div class="sprocket-quotes-item quotes-bottomleft-arrow">
                      <p class="sprocket-quotes-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                      <div class="sprocket-quotes-info"> <img src="images/4.png" class="sprocket-quotes-image" alt="image"> <span class="sprocket-quotes-author"> Samantha Jones </span> <span class="sprocket-quotes-subtext"> Lives in: Vienna </span> </div>
                    </div>
						</div>
                    </div>
                    <div class="item">
                        <div class="col-md-6 col-sm-6">
        				    <div class="sprocket-quotes-item quotes-bottomright-arrow">
                      <p class="sprocket-quotes-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                      <div class="sprocket-quotes-info"> <img src="images/1.png" class="sprocket-quotes-image" alt="image"> <span class="sprocket-quotes-author"> Michael Buffalo </span> <span class="sprocket-quotes-subtext"> Lives in: New York </span> </div>
                    </div>
						</div>
<div class="hidden-xs">
						    
													</div>
						<div class="col-md-6 col-sm-6 hidden-sm hidden-xs">
							<div class="sprocket-quotes-item quotes-bottomleft-arrow">
                      <p class="sprocket-quotes-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                      <div class="sprocket-quotes-info"> <img src="images/2.png" class="sprocket-quotes-image" alt="image"> <span class="sprocket-quotes-author"> Nora Martinez </span> <span class="sprocket-quotes-subtext"> Lives in: Paris </span> </div>
                    </div>
						</div>
                    </div>
                                     
                </div>
				
                <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev" style="color:white;">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next" style="color:white;">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
</section>
<section id="verticalDriver" data-type="background" data-speed="5">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="header header-driver">TAXI DRIVER FOR HIRE</h3>
      </div>
    </div>
  </div>
  <div id="drivers" class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="uk-panel uk-panel-box">
          <div class="uk-overlay"> <img alt="" src="images/driver1.png">
            <div class="uk-overlay-area">
              <div class="uk-overlay-area-content">
                <p><a href="#">full-time driver</a></p>
                <p> will drive top of the line taxi in the streets of london </p>
                <p><a href="#" class="btn btn-primary btn-inverse btn-sm">Read More</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="uk-panel uk-panel-box">
          <div class="uk-overlay"> <img alt="" src="images/driver2.png">
            <div class="uk-overlay-area">
              <div class="uk-overlay-area-content">
                <p><a href="#">night watch driver</a></p>
                <p>we're looking for a driver working mostly nights at the airport</p>
                <p><a href="#" class="btn btn-primary btn-inverse btn-sm">Read More</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="uk-panel uk-panel-box">
          <div class="uk-overlay"> <img alt="" src="images/driver3.png">
            <div class="uk-overlay-area">
              <div class="uk-overlay-area-content">
                <p><a href="#">part-time driver</a></p>
                <p> we need a guy who can step up when the all lines are busy</p>
                <p><a href="#" class="btn btn-primary btn-inverse btn-sm">Read More</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="uk-panel uk-panel-box">
          <div class="uk-overlay"> <img alt="" src="images/driver4.png">
            <div class="uk-overlay-area">
              <div class="uk-overlay-area-content">
                <p><a href="#">big apple cab driver</a></p>
                <p> will drive mercedes benz gl for our top customers </p>
                <p><a href="#" class="btn btn-primary btn-inverse btn-sm">Read More</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section white">
  <div class="inner">
    <h1 class="main-heading">Our luxurious cars</h1>
    <div id="_featuredslider" class="owl-carousel featured-cars">
      <div class="item">
        <div class="featured-car">
          <div class="image"> <a href="#"><img src="images/slider/executive_car.jpg" alt="executive_car" class="img-responsive"></a> </div>
          <div class="content">
            <div class="clearfix">
              <h5><a href="#">MPV </a></h5>
              <span class="price">&euro;22 car rate</span> </div>
            <div class="line"></div>
            <p class="desc"> It's the best Luxurious Business car that suits your personality.</p>
          </div>
          <div class="details clearfix">
            <div class="baby-seat pop1-1">
              <div class="popup-new popup1 popup-1-1">Baby Seat</div>
              <i class="icon-car-seat"></i></div>
            <div class="extra-luggage pop1-3 pop1-3">
              <div class="popup-new popup3 popup-1-3">Access Luggage</div>
              <i class="fa fa-suitcase"></i></div>
          </div>
          <style>
.popup-1-1{
display:none;
}
.popup-1-3{
display:none;
}
.pop1-1:hover .popup-1-1 {
display:block;
}
.pop1-3:hover .popup-1-3 {
display:block;
}
</style>
        </div>
      </div>
      <div class="item">
        <div class="featured-car">
          <div class="image"> <a href="#"><img src="images/slider/mpv_8seater_car.jpg" alt="mpv_8seater_car" class="img-responsive"></a> </div>
          <div class="content">
            <div class="clearfix">
              <h5><a href="#">MPV </a></h5>
              <span class="price">&euro;22 car rate</span> </div>
            <div class="line"></div>
            <p class="desc"> It's the best Luxurious Business car that suits your personality.</p>
          </div>
          <div class="details clearfix">
            <div class="baby-seat pop1-1">
              <div class="popup-new popup1 popup-1-1">Baby Seat</div>
              <i class="icon-car-seat"></i></div>
            <div class="extra-luggage pop1-3 pop1-3">
              <div class="popup-new popup3 popup-1-3">Access Luggage</div>
              <i class="fa fa-suitcase"></i></div>
          </div>
          <style>
.popup-1-1{
display:none;
}
.popup-1-3{
display:none;
}
.pop1-1:hover .popup-1-1 {
display:block;
}
.pop1-3:hover .popup-1-3 {
display:block;
}
</style>
        </div>
      </div>
      <div class="item">
        <div class="featured-car">
          <div class="image"> <a href="#"><img src="images/slider/saloon_car.jpg" alt="saloon_car" class="img-responsive"></a> </div>
          <div class="content">
            <div class="clearfix">
              <h5><a href="#">MPV </a></h5>
              <span class="price">&euro;22 car rate</span> </div>
            <div class="line"></div>
            <p class="desc"> It's the best Luxurious Business car that suits your personality.</p>
          </div>
          <div class="details clearfix">
            <div class="baby-seat pop1-1">
              <div class="popup-new popup1 popup-1-1">Baby Seat</div>
              <i class="icon-car-seat"></i></div>
            <div class="extra-luggage pop1-3 pop1-3">
              <div class="popup-new popup3 popup-1-3">Access Luggage</div>
              <i class="fa fa-suitcase"></i></div>
          </div>
          <style>
.popup-1-1{
display:none;
}
.popup-1-3{
display:none;
}
.pop1-1:hover .popup-1-1 {
display:block;
}
.pop1-3:hover .popup-1-3 {
display:block;
}
</style>
        </div>
      </div>
      <div class="item">
        <div class="featured-car">
          <div class="image"> <a href="#"><img src="images/slider/estate_car.jpg" alt="estate_car" class="img-responsive"></a> </div>
          <div class="content">
            <div class="clearfix">
              <h5><a href="#">MPV </a></h5>
              <span class="price">&euro;22 car rate</span> </div>
            <div class="line"></div>
            <p class="desc"> It's the best Luxurious Business car that suits your personality.</p>
          </div>
          <div class="details clearfix">
            <div class="baby-seat pop1-1">
              <div class="popup-new popup1 popup-1-1">Baby Seat</div>
              <i class="icon-car-seat"></i></div>
            <div class="extra-luggage pop1-3 pop1-3">
              <div class="popup-new popup3 popup-1-3">Access Luggage</div>
              <i class="fa fa-suitcase"></i></div>
          </div>
          <style>
.popup-1-1{
display:none;
}
.popup-1-3{
display:none;
}
.pop1-1:hover .popup-1-1 {
display:block;
}
.pop1-3:hover .popup-1-3 {
display:block;
}
</style>
        </div>
      </div>
      <div class="item">
        <div class="featured-car">
          <div class="image"> <a href="#"><img src="images/slider/mpv.jpg" alt="mpv" class="img-responsive"></a> </div>
          <div class="content">
            <div class="clearfix">
              <h5><a href="#">MPV </a></h5>
              <span class="price">&euro;22 car rate</span> </div>
            <div class="line"></div>
            <p class="desc"> It's the best Luxurious Business car that suits your personality.</p>
          </div>
          <div class="details clearfix">
            <div class="baby-seat pop1-1">
              <div class="popup-new popup1 popup-1-1">Baby Seat</div>
              <i class="icon-car-seat"></i></div>
            <div class="extra-luggage pop1-3 pop1-3">
              <div class="popup-new popup3 popup-1-3">Access Luggage</div>
              <i class="fa fa-suitcase"></i></div>
          </div>
          <style>
.popup-1-1{
display:none;
}
.popup-1-3{
display:none;
}
.pop1-1:hover .popup-1-1 {
display:block;
}
.pop1-3:hover .popup-1-3 {
display:block;
}
</style>
        </div>
      </div>
      <div class="item">
        <div class="featured-car">
          <div class="image"> <a href="#"><img src="images/slider/mpv_7seater_car.jpg" alt="mpv_7seater_car" class="img-responsive"></a> </div>
          <div class="content">
            <div class="clearfix">
              <h5><a href="#">MPV </a></h5>
              <span class="price">&euro;22 car rate</span> </div>
            <div class="line"></div>
            <p class="desc"> It's the best Luxurious Business car that suits your personality.</p>
          </div>
          <div class="details clearfix">
            <div class="baby-seat pop1-1">
              <div class="popup-new popup1 popup-1-1">Baby Seat</div>
              <i class="icon-car-seat"></i></div>
            <div class="extra-luggage pop1-3 pop1-3">
              <div class="popup-new popup3 popup-1-3">Access Luggage</div>
              <i class="fa fa-suitcase"></i></div>
          </div>
          <style>
.popup-1-1{
display:none;
}
.popup-1-3{
display:none;
}
.pop1-1:hover .popup-1-1 {
display:block;
}
.pop1-3:hover .popup-1-3 {
display:block;
}
</style>
        </div>
      </div>
    </div>
  </div>
</section>


<?php include("footer.php"); ?>
