<?php include("header.php"); ?>
<?php include("php/SessionUnset.php"); ?>
<?php include("banner_inner.php"); ?>

<div class="container about_container">
  <h2>Privacy Policy</h2>
 	<p>Your privacy is our priority. We are committed to protecting your personal information at all times, in all situations. The information we collect about you is used to process your booking and to provide a more personalized experience on our site.</p>
	<h4><b>Use Of Information?</b></h4>
 	<p>When you become london-2-airport.com customer or member, we ask you to fill out a form that requests some personal details, including your name and address. Your name and address information is used solely to process bookings unless your prior consent is given. This information will never be sold, given, rented or traded to others for purposes other than order fulfilment.</p>
	<h4><b>Our Use of Log Files</b></h4>
	<p>When you visit our site we automatically log your IP address, your browser type and your access times. We utilize this information to conduct site performance evaluations, to see where visitors are coming from and to keep track of click stream data (the screens our users visit on our site). This data helps us to determine what content our members find most appealing so that we can maximize your enjoyment of the site. Log files are not tied to personally identifiable information.</p>
	<h4><b>Site Security</b></h4>
	<p>All london-2-airport.com employees take your personal information very seriously. All employees must learn and obey our security policies. Access personal information is limited to key management personnel. The information is ID and password protected and our Web security is reviewed regularly.</p>
	<h4><b>Correcting/Updating Your Personal Information</b></h4>
	<p>After becoming , our member you may correct/update your personal information by clicking on the 'My Account' link at the top of any page, entering your ID and password when prompted, and clicking the Edit Information button. The changes you make will be reflected in our databases instantly. If you wish to deactivate your account, you may contact our member services staff through the 'Contact Us' section.</p>
	<h4><b>Links To Other Sites</b></h4>
	<p>This Web site contains links to other Web sites. Please note that when you click on one of these links, you may be leaving site and entering another Web site. These sites may independently collect or request information about you and we encourage you to read the privacy statements of these linked sites as their privacy policy may differ from ours.</p>
	<h4><b>Communication</b></h4>
	<p>From time to time we may send out email messages alerting you that products are available, to remind you about providing feedback or for promotional offers. In addition, you will receive communication from us 1) if you contact us for a particular reason, 2) to receive your bookings, or 3) we need to contact you for administrative purposes. Administrative e-mails will not contain promotional materials. If you choose to receive our Newsletters, they will contain promotional materials from </p>
	<h4><b>Legal Disclaimer</b></h4>
	<p>Though we make every effort to preserve user privacy, we may need to disclose personal information when required by law wherein we have a good-faith belief that such action is necessary to comply with a current judicial proceeding, a court order or legal process served on our Web site</p>
	<h4><b>Supplementation</b></h4>
	<p>We may correct or supplement your address information with standardized postal representation in order to ensure that your orders are delivered. We do this since we want to make sure that our members receive the order and to lower costs associated with our programs.</p>
	<h4><b>Our Use of Cookies</b></h4>
	<p>Cookies are pieces of information that your browser stores on your computer's hard drive which identify you when you enter. We may use session cookies to provide a seamless experience on the site and to combine with our log files so that we can understand our site traffic and analyze our demographic information in aggregate form.</p>
	<h4><b>Your Feedback is Always Welcome</b></h4>
	<p>We welcome your comments and questions about our privacy policy - or anything else you might want to talk about. To contact us<a href="contact.php"> click here.</a></p>
	
</div>
<?php include("footer.php"); ?>
