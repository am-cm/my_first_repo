<?php 
include("config/config.php"); 
session_start();
error_reporting(0);
$user_email=$_SESSION['login_user'];

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="SityCab is a taxi company responsive html template.">
<meta name="keywords" content="boostrap, responsive, html5, css3, jquery, theme, uikit, multicolor, parallax, retina, taxi business" />
<meta name="author" content="dhsign">
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="3 days" />
<title>CityCab - Taxi Company Responsive Html Template</title>


<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="fonts/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
<link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/uikit.almost-flat.css" rel="stylesheet" type="text/css">
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="css/owl.theme.css" rel="stylesheet" type="text/css">
<link href="css/quotes.css" rel="stylesheet" type="text/css">
<link href="css/product.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="citycab.css" rel="stylesheet" type="text/css">
<link href="styles.css" rel="stylesheet" type="text/css">
<link href="css/mystyle.css" type="text/css" rel="stylesheet" >

<div id="google_translate_element"></div>
<script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $(".myurl").click(function(){
        var x = document.URL;
        $(".myurlform").attr("action","php/process_login.php");
        $('.myurlform').append('<input name="url" type="hidden" value="'+ x +'">');
        $('body').find('.myurlform').submit();
    });
});
</script>
</head>
<body>
<div class="page-wrapper" id="page-top">
<header id="header-wrapper">
  <div class="container main-navigation">
    <div id="header" class="row">
      <div class="col-md-12 col-pad-0">
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
          <div class="navbar-header"> <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"></a> </div>
          <div class="navbar-brand"> <a href="index.php">
		  <?php $query=mysql_query("SELECT * FROM `seting`"); 
		  $row=mysql_fetch_array($query);
		  ?>
		  <img src="upload/logo/<?php echo $row['logo_img']; ?>" alt="Logo" class="img-circle img-responsive header_logo"></a> </div>
          <div class="navigation clearfix animated fadeInRight">
            <h4 class="contact_header"><i class="fa fa-mobile" aria-hidden="true"></i> <a href="">1234567890</a> </h4>
            <div class="login">  
			<?php
			if($user_email != "")
					{
			?>
			<a href="php/logout.php" class="logout btn switch-btn">Logout</a>
			<?php } else { ?>
			
      <a href="#" class="logout btn switch-btn" id="login_popover" data-target="#login_box" data-toggle="modal"><i class="ion-ios-person"></i></a>
			 <span class="tooltiptext"><div class="arrow-left"></div>Login / Signup</span>
			<?php } ?>
			
			</div>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-nav-center">
              <li><a href="index.php">Home</a></li>
              <li><a href="booknow.php">Book Now</a></li>
              <li data-uk-dropdown="" class="uk-parent"><a href="#">Our Fleet <i class="-caret-down"></i></a>
                <div class="uk-dropdown uk-dropdown-navbar" style="">
                  <ul class="uk-nav uk-nav-navbar">
                    <li><a href="#">Our Drivers</a></li>
                    <li><a href="#">Our Vehicles</a></li>
                  </ul>
                </div>
              </li>
              <li data-uk-dropdown="" class="uk-parent"><a href="#">Our Specialties <i class="uk-icon-caret-down"></i></a>
                <div class="uk-dropdown uk-dropdown-navbar" style="">
                  <ul class="uk-nav uk-nav-navbar">
                    <li><a href="#">Airport Transfer Services</a></li>
                    <li><a href="#">Minicab - Door To Door Services</a></li>
                    <li><a href="#">Day Tour</a></li>
                    <li><a href="#">London Magical Tour</a></li>
                    <li><a href="#">School Runs</a></li>
                    <li><a href="#">Daily Office Runs</a></li>
                    <li><a href="#">Hospital Appointments</a></li>
                    <li><a href="#">Wedding / Event Transfers</a></li>
                    <li><a href="#">Courier AND Parcel Delivery</a></li>
                  </ul>
                </div>
              </li>
              <li data-uk-dropdown="" class="uk-parent"><a href="#">Join The Family <i class="uk-icon-caret-down"></i></a>
                <div class="uk-dropdown uk-dropdown-navbar" style="">
                  <ul class="uk-nav uk-nav-navbar">
                    <li><a href="driver_requirement.php">Driver Recruitment</a></li>
                    <li><a href="#">Staff Recruitment</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<div id="taxiStripe" class="container-fluid gap-fullsize"> </div>