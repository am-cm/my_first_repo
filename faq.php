<?php include("header.php"); ?>
<?php include("php/SessionUnset.php"); ?>
<?php include("banner_inner.php"); ?>
<style>
.panel-title {
font-size: 20px;
text-transform: none;
font-weight: 400;
padding: 0;
position: relative;
}

.panel-title > a {
font-size: 14px;
text-transform: uppercase;
display: block;
font-weight: 700;
padding: 10px 28px 25px 24px;
background-color: #073A29;
color: #fff !important;
}

.panel-title > a:after {
color: #fff;
content: '-';
position: absolute;
font-size: 22px;
right: 27px;
top: 13px;
}

.panel-title > a:hover {
background-color: #073A29;
}

.panel-title > a.collapsed {
background-color: #073A29;
}

.panel-title > a.collapsed:hover {
background-color: #073A29;
}

.panel-title > a.collapsed:after {
content: '+';
right: 24px;
}

.panel {
box-shadow: none;
}

.panel-group .panel {
border-radius: 0;
background-color: transparent;
}

.panel-default > .panel-heading {
background-color: transparent;
color: inherit;
position: relative;
border: none;
border-radius: 0;
padding: 0;
}

.panel-heading {
padding: 16px 0px 14px 16px;
}

.panel-heading[class*="rt-icon-"]:before {
position: absolute;
font-size: 20px;
color: #e16657;
left: 16px;
}

.panel-default {
border-color: transparent;
}

.panel-default + .panel-default {
border: 0;
}

.panel-group .panel + .panel {
margin-top: 9px;
}

.panel-group .panel-heading + .panel-collapse .panel-body {
padding-top: 20px;
padding-bottom: 20px;
padding-left: 30px;
border-right: 1px solid #eee;
border-left: 1px solid #eee;
border-bottom: 1px solid #eee;
border-top: none;
}
</style>
<section id="content" class="section">
<div class="content-wrap">
  <div class="container faq_container">
    <div class="col-md-12">
      <h3 class="faq_h3">
        <center>
          <b>FAQ</b>
        </center>
      </h3>
      <div class="bs-example">
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><b>Question 1?</b></a> </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
              <div class="panel-body">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapsetwo"><b>Question 2?</b></a> </h4>
            </div>
            <div id="collapsetwo" class="panel-collapse collapse">
              <div class="panel-body">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree"><b>Question 3?</b></a> </h4>
            </div>
            <div id="collapsethree" class="panel-collapse collapse">
              <div class="panel-body">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour"><b>Question 4?</b></a> </h4>
            </div>
            <div id="collapsefour" class="panel-collapse collapse">
              <div class="panel-body">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapsefive"><b>Question 5?</b></a> </h4>
            </div>
            <div id="collapsefive" class="panel-collapse collapse">
              <div class="panel-body">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<?php include("footer.php"); ?>
