<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
 
<?php include('header.php'); ?>                    
                   
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">View</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Driver</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <a class="btn red btn-outline btn-primary" href="add_user.php">Create New Driver</a>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Email </th>
                                                <th> Roll </th>
                                                <th> status </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
        <?php
            $query=mysql_query("SELECT user.* , driver_personal_info.title FROM  user join driver_personal_info on user.id=driver_personal_info.user_id where user_type='driver'");
            while($user=mysql_fetch_array($query))
            {
        ?>
                                            <tr>
                                                <td><?php echo $user['title'] . " " . $user['firstname']." ".$user['lastname']; ?></td>
                                                <td> <?php echo $user['email']; ?> </td>
                                                <td> <?php echo $user['user_type']; ?></td>
                                                <td><?php echo $user['status']; ?></td>
                                                <td> <a href="driver_profile.php?id=<?php echo $user['id']; ?>" class="btn purple"> Edit
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="javascript:;" class="btn red">
                                                        <i class="fa fa-times"></i> Delete
                                                    </a>
                                                </td>
                                            </tr>
        <?php
            }
        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                </div>
                <!-- END CONTENT BODY -->

    <?php include('footer.php'); ?>

<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>