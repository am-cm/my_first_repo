<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
 <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function()
{
      
    $(".approve").click(function()
    {
        var id=$(this).val();
        var dataString = 'did='+ id;
        //var=userid=document.getElementsByTagName('user_id').val();
        //var dataString = 'uid='+ userid;
    
        $.ajax
        ({
            type: "POST",
            url: "php/approvedoc.php",
            data: dataString,
            cache: false,
            success: function(html)
            {
                window.location.assign("driver_profile.php?id=<?php echo $_GET['id']; ?>");
            } 
        });
    });

    
    $(".reject").click(function()
    {
        var id=$(this).val();
        var dataString = 'did='+ id;
        //var=userid=document.getElementsByTagName('user_id').val();
        //var dataString = 'uid='+ userid;
        var retVal = prompt("Enter your reason : ", "");

    
        $.ajax
        ({
            type: "POST",
            url: "php/reject.php?reason=" + retVal,
            data: dataString,
            cache: false,
            success: function(html)
            {
                window.location.assign("driver_profile.php?id=<?php echo $_GET['id']; ?>");
            } 
        });
    });

    
});
</script>
   
<?php include('header.php'); ?>
   
<?php
    $id=$_GET['id'];
    $query=mysql_query("SELECT user.image as img,user.firstname,user.lastname, driver_vehicle_info.*, driver_personal_info.* FROM  user JOIN driver_personal_info on user.id=driver_personal_info.user_id JOIN driver_vehicle_info on user.id=driver_vehicle_info.user_id where user.id=$id");
    $driver=mysql_fetch_array($query);
?>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Driver Profile</span>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="profile.php?id=<?php echo $id; ?>">
                                            <i class="icon-user"></i> <?php echo $driver['firstname']; ?> profile</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="profile">
                        <div class="tabbable-line tabbable-full-width">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab"> Driver Detels... </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <ul class="list-unstyled profile-nav">
                                                <li>
                                                    <a href="javascript:;" > <?php echo $driver['title'] .' ' . $driver['firstname'] .' '.$driver['lastname']; ?> </a>
                                                </li>
                                                <li>
                                                <?php if($driver['img'] !=""){ ?>
                                                    <img src="../upload/user/<?php echo $driver['img']; ?>" class="img-responsive pic-bordered" alt="" />
                                                <?php }else{ ?>
                                                    <img src="..\assets\global\img\adduser.png" alt="" style="width:100%">
                                                <?php } ?>
                                                    
                                                </li>
                                            </ul>
                                            <ul class="list-unstyled profile-nav">
                                                <li>
                                                    <a href="javascript:;" >Vehicle Image....</a>
                                                </li>
                                                <li>
                                                <?php if($driver['image'] !=""){ ?>
                                                    <img src="../upload/vehicle/<?php echo $driver['image']; ?>" class="img-responsive pic-bordered" alt="" />
                                                <?php }else{ ?>
                                                    <img src="..\assets\global\img\adduser.png" alt="" style="width:100%">
                                                <?php } ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-8 profile-info">
                                                    
                                                </div>
                                                <!--end col-md-8-->
                                               
                                                <!--end col-md-4-->
                                            </div>
                                            <!--end row-->
                                            <div class="tabbable-line tabbable-custom-profile">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#tab_1_11" data-toggle="tab"> Personal Information</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_22" data-toggle="tab"> Vehicle Information</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_33" data-toggle="tab"> Attach Documents </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_44" data-toggle="tab"> Documents Status</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1_11">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            Heading 
                                                                        </th>
                                                                        <th class="hidden-xs">
                                                                            Descrition 
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody> 
                                                                    <tr>
                                                                        <td>
                                                                             Date of brithday: 
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="dob" value="<?php echo $driver['dob']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             N I Number:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="tetx" name="ni_no" value="<?php echo $driver['ni_no']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             DVLA No:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="tetx" name="dvla_no" value="<?php echo $driver['dvla_no']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             PCO Badge:   
                                                                        </td>
                                                                        <td> 
                                                                            <input type="tetx" name="pco" value="<?php echo $driver['pco']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Document Approve Status:   
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($driver['document_approve_status'] == 1){ ?>
                                                                            <i class="fa fa-check"></i>
                                                                        <?php } else { ?>
                                                                            <i class="fa fa-close"></i>
                                                                        <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Payment Approve Status:   
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($driver['Payment_approve_status'] == 1){ ?>
                                                                            <i class="fa fa-check"></i>
                                                                        <?php } else { ?>
                                                                            <i class="fa fa-close"></i>
                                                                        <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Vehicle Approve Status:   
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($driver['vehicle_approve_status'] == 1){ ?>
                                                                            <i class="fa fa-check"></i>
                                                                        <?php } else { ?>
                                                                            <i class="fa fa-close"></i>
                                                                        <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                     
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!--tab-pane-->
                                                <div class="tab-pane" id="tab_1_22">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <i class="fa fa-briefcase"></i> Heading </th>
                                                                        <th class="hidden-xs">
                                                                            <i class="fa fa-question"></i> Descrition </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                             Registration:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="registration" value="<?php echo $driver['registration']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Make:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="make" value="<?php echo $driver['make']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Model:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="model" value="<?php echo $driver['model']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Vehicle Type:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="vehicle_type" value="<?php echo $driver['vehicle_type']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Vehicle Color:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="vehicle_color" value="<?php echo $driver['vehicle_color']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Vehicle passanger_capacity:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="passanger_capacity" value="<?php echo $driver['passanger_capacity']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Beg 20kg capacity:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="beg_20kg" value="<?php echo $driver['beg_20kg']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Beg 5kg capacity:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="beg_5kg" value="<?php echo $driver['beg_5kg']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="hidden-xs" colspan="2"  style="text-align: center;">Owner Detels </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tr>
                                                                        <td>
                                                                            name:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="owner_name" value="<?php echo $driver['owner_name']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             1st line address:    
                                                                        </td>
                                                                        <td> 
                                                                            <textarea class="form-control" rows="3" placeholder="Vehicle Registered Address Line One!!!" name="address1" class="form-control"><?php echo $driver['vehicle_registered_address1']; ?></textarea>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             2st line address:    
                                                                        </td>
                                                                        <td> 
                                                                            <textarea class="form-control" rows="3" placeholder="Vehicle Registered Address Line Two!!!" name="address2" class="form-control"><?php echo $driver['vehicle_registered_address2']; ?></textarea>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Postcode:  
                                                                        </td>
                                                                        <td> 
                                                                            <input type="post_code" name="title" value="<?php echo $driver['post_code']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_33">
                                                        <div class="portlet-body">
                                                        <form action="php/approvedoc.php" method="post">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th> Name </th>
                                                                        <th>Type </th>
                                                                        <th>Expiry Date </th>
                                                                        <th>Approve </th>
                                                                        <th>Action </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                    $q=mysql_query("SELECT * FROM documents where user_id=$id");
                                                                    while($documents=mysql_fetch_array($q))
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             <?php echo $documents['name']; ?>
                                                                        </td>
                                                                        <td> 
                                                                            <?php echo $documents['type']; ?>
                                                                        </td>
                                                                         <td> 
                                                                            <?php echo $documents['expiry_date']; ?>
                                                                        </td>
                                                                        <td>
                                                                        <?php if($documents['status'] == 1){ ?>
                                                                            <i class="fa fa-check"></i>
                                                                        <?php }elseif ($documents['status'] == 2) {?>
                                                                            
                                                                            <div class="md-checkbox">
                                                                            <i class="fa fa-close"></i>
                                                                            <input type="checkbox" id="checkbox<?php echo $documents['id']; ?>" class="md-check approve" name="<?php echo $documents['name']; ?>" value="<?php echo $documents['id']; ?>">
                                                                            <label for="checkbox<?php echo $documents['id']; ?>">
                                                                                <span></span>
                                                                                <span class="check"></span>
                                                                                <span class="box"></span> </label>
                                                                            </div>
                                                                        <?php }else{ ?>
                                                                        <div class="md-checkbox">
                                                                            <input type="checkbox" id="checkbox<?php echo $documents['id']; ?>" class="md-check approve" name="<?php echo $documents['name']; ?>" value="<?php echo $documents['id']; ?>">
                                                                            <label for="checkbox<?php echo $documents['id']; ?>">
                                                                                <span></span>
                                                                                <span class="check"></span>
                                                                                <span class="box"></span> </label>
                                                                        </div>
                                                                        <?php } ?>
                                                                        </td>
                                                                        <td> 

                                                                            <a class="btn btn-sm grey-salsa btn-outline" open target="_blank" href="../upload/driver_document/<?php if($documents['type'] =='Vehicle'){echo 'vehicle';}else{echo 'Personal';} ?>/<?php echo $documents['document']; ?>"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane" id="tab_1_44">
                                                        <div class="portlet-body">
                                                        <form action="php/approvedoc.php" method="post">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th> Name </th>
                                                                        <th>Type </th>
                                                                        <th>Expiry Date </th>
                                                                        <th>Rejeced </th>
                                                                        <th>Action </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                    $q=mysql_query("SELECT * FROM documents where user_id=$id AND status='1' or status='2'");
                                                                    while($documents=mysql_fetch_array($q))
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             <?php echo $documents['name']; ?>
                                                                        </td>
                                                                        <td> 
                                                                            <?php echo $documents['type']; ?>
                                                                        </td>
                                                                         <td> 
                                                                            <?php echo $documents['expiry_date']; ?>
                                                                        </td>
                                                                        <td>
                                                                        <?php if($documents['status'] == 2){ ?>
                                                                            <i class="fa fa-check"></i>
                                                                        <?php }else{ ?>
                                                                         <div class="md-checkbox">
                                                                            <input type="checkbox" id="checkbox2_<?php echo $documents['id']; ?>" class="md-check reject" name="<?php echo $documents['name']; ?>" value="<?php echo $documents['id']; ?>">
                                                                            <label for="checkbox2_<?php echo $documents['id']; ?>">
                                                                                <span></span>
                                                                                <span class="check"></span>
                                                                                <span class="box"></span> </label>
                                                                        </div>
                                                                        <?php } ?>
                                                                        </td>
                                                                        <td> 
                                                                            <a class="btn btn-sm grey-salsa btn-outline" open target="_blank" href="../upload/driver_document/<?php if($documents['type'] =='Vehicle'){echo 'vehicle';}else{echo 'Personal';} ?>/<?php echo $documents['document']; ?>"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!--tab-pane-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                                         </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
<?php include('footer.php'); ?>

<script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>

<script src="../assets/pages/scripts/components-date-time-pickers.min.js"></script>
<script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
